.. _venue checklist:

Venue checklist
===============

When organising a DebConf, there are a number of things the local team needs to
investigate so that the video team knows what to expect when they arrive to
set-up. Most of these are physical constraints of the rooms hired, which makes
pictures of the rooms and existing equipment useful.

The biggest question is how large the rooms are. This affects the :ref:`room
setup`. The positioning and number of power and network sockets as well as the
speed of the network sockets are also crucial. If there are floor plans of the
rooms, they make planning of the setup much easier.

To stream the conference, we need at least a network uplink with a 5mbps
dedicated connection. Although not a requirement, having at least a single
static public IP that we can NAT behind is appreciated. If we can get a range
of IPs this makes things easier. Our setup requires gigabit connectivity
between all our machines.

Another consideration is how cables can be run between the front and the back of
the talk rooms. This includes any local safety laws that may prevent us from
running cables in specific areas or tape them down to the floor. If there is a
false ceiling that cables can be hung from, this is often the easiest solution.

Provided equipment
------------------

Often the chosen venue has some audio/visual equipment available for use to use
in the talk rooms. What is available needs to be determined early on so that the
hire list can be adjusted as needed.

The most important piece of equipment usually provided by the venue is one (or
more) projector(s) in each talk room. These need to be able to take HDMI input,
as this is what the Opsis outputs. They should also be of sufficient quality so
that the image projected is clearly visible. If there are any switchers or range
extenders between the computer input and the projector, these should also be
identified.

The next useful pieces of equipment are speakers. We can hire these, but it is
easier if the venue has a room PA that we can connect into. We require stereo
(left and right) 3 pin `XLR`_ or balanced `1/4" jack`_ line inputs that we can
connect our mixer into.

Finally we need computers. These are usually easier to borrow from the venue
than to hire. We need servers on site to handle encoding, storage and streaming
as well as one desktop per room for mixing the video.

.. _`XLR`: https://en.wikipedia.org/wiki/XLR_connector
.. _`1/4" jack`: https://en.wikipedia.org/wiki/Phone_connector_(audio)

Checklist
---------

Now that you understand why we need these things, here is a checklist you can
use to provide us with the information we need:

- Floor plans (if possible)
- Detailed pictures of the rooms
- Detailed pictures of the existing audio and video equipment in the rooms
- Position of the power sockets
- Position and speed of the network sockets
- Network uplink speed
- Network and firewall topology
- Review of the local safety laws with regards to cable runs and large objects
  (tables, etc.) in walkways
- Plans on how to provide us with computers and servers
