.. _advice for recording:

Advice for recording presentations
==================================

This is the reference documentation for presenters submitting pre-recorded talks
for online conferences managed by the DebConf Video Team. If you think something
should be included here, don’t hesitate to :ref:`contact us` or to make a merge
request on the `git repository for this documentation`__.

This documentation will focus on tools and formatting guidelines to ensure that
your presentation is easy to read and hear during the conference.

If you run into any problems or have questions about recording your
presentation, feel free to ask us in the :code:`#dc20-speakerdesk` IRC channel
on OFTC and we will try help you.

__ https://salsa.debian.org/debconf-video-team/docs


Cheatsheet
----------

Please use the following:

- a microphone
- :code:`16:9` aspect ratio for your slides
- large fonts so people on slow connections can see
- :code:`1280x720` resolution
- the correct frame rate according to your country's electrical frequency (25 or
  30 fps - divide the frequency by two) - `Wikipedia Chart`_
- headphones if you are in a multi-presenter session, or you will cause an echo
  for everybody

.. _Wikipedia Chart: https://en.wikipedia.org/wiki/Mains_electricity_by_country#Table_of_mains_voltages,_frequencies,_and_plugs

And remember to leave space for your face if you want to show it at the same
time as your slides.

Slides
------
We stream video with an aspect ratio of :code:`16:9`. Thus, your slides should
also be :code:`16:9` to avoid black bars on either side in the stream. In
LibreOffice you can change this for your slides by, clicking on Slide ->
Properties and then adjusting it in the Format field. You should also use large
fonts, as attendees with slow internet connections may use video qualities as
low as :code:`480x270`.

Many presenters choose to place the video of themselves talking at the bottom
right corner of the screen, while their slides are shown full-screen. If you
intend to do so, leave a space in your slides so that your face does not cover
up any of the slides' content.

Recording
---------

There are many tools for recording your screen that you can use to record your
talk. This guide will describe several in detail and give you links to others
if you do not find they suit your needs. Please record :code:`1280x720` video
(25/30 fps) at a bitrate of at least 2Mbps. The frame-rate is determined by
your country's electrical frequency (`Wikipedia Chart`_) if you are using
artificial lighting, otherwise you should use 30 fps, as this is what we are
streaming at.

Before you record your talk, make sure you have a light source in front of you,
as a light source behind you will make it difficult to see your face in the
video. This video contains some good tips on positioning and lighting:
https://www.youtube.com/watch?v=tEC8q9i2fOw.

It is also a good idea to try a short test recording, and check if the lights
cause the background to flicker.  If this happens, try a different frame-rate
(30 vs 25). Also check your audio and make sure that there is no unwanted noise
and that it is intelligible.

As this is a talk, the audience needs to hear you even more than they need to
see you. Thus, use an external microphone, even if it just the one on your
phone's headphones. Check the audio level in the software you are using to
record. It should come close to -3dB on the meter (typically coloured yellow)
without going above 0dB (red coloured in most recording software). If you are
recording a BoF or a talk with multiple presenters, everyone should wear
headphones to ensure that there is no echo in the recording.

OBS
^^^

OBS is a fully featured tool for recording and processing video. It supports
multiple layouts, that you can switch between during your talk. There is a
slight learning curve to using it, but there is a lot of documentation on the
Internet for it. It can be installed on all supported versions of Debian::

  apt install obs-studio

Recording your face can be done using a webcam. If you want better quality, you
can use your phone camera, but will introduce some latency between when you say
something and it appearing in OBS. You can stream your phone camera using
`Spydroid`_ (free), `Larix`_ (non-free), or `Droidcam`_ (non-free).

.. _`Spydroid`: https://f-droid.org/en/packages/net.majorkernelpanic.spydroid/
.. _`Larix`: https://softvelum.com/larix/
.. _`Droidcam`: https://play.google.com/store/apps/details?id=com.dev47apps.droidcam

Vokoscreen
^^^^^^^^^^

Vokoscreen is a GUI tool for easy screencast recording. It is much more simple
when compared to OBS, but can do the job well. It can capture the full screen or
a portion of it. It has a feature to overlay your webcam in the corner of the
screen which you should enable if you wish to record your face. In Debian Buster
and earlier, the package is called ``vokoscreen``. In Debian Bullseye and newer,
it has been replaced with ``vokoscreen-ng``.

FFmpeg Capture
^^^^^^^^^^^^^^

You can record your entire screen and the default pulseaudio input with this
FFmpeg command::

  ffmpeg -f x11grab -framerate 30 -video_size 1280x720 -i :0.0 -f pulse -ac 2 -i default -b:v 2M output.mkv

Editing
-------

The biggest thing about editing is not to spend too much time doing it, but
concentrate on the talk's content instead. The recording edit should just trim
the start and end, so that the talk starts and the beginning and finishes at the
end and remove any long interruptions during the talk. There are many tools to
edit video, we recommend Kdenlive (Buster and later), Shotcut (Bullseye and
later) and Openshot (``openshot-qt``) which is in Buster and later Debian
releases.

Your final video should have the following settings (some of these may need to
be set when you start the editing project):

* Resolution: :code:`1280x720`
* Bitrate: 2Mbps
* Video format: VP9 or H.264
* Audio format: Opus, Vorbis, AAC or MP3 (with a high enough bitrate so that it
  does not sound terrible)
* Audio channels: Mono or stereo
* Container: WebM, Matroska (MKV) or MP4

Please review your video after exporting it to ensure that the result is what
you expect.

Tips and Tricks
---------------

* A virtual X output can let you see your presenter notes while recording your
  presentation: https://hashman.ca/libreoffice/
* If you are not in front of your computer, a confidence monitor near the camera
  is a great way to make sure the video recording is going according to plan.
* There are several tools to help take animated gifs and screenshots for
  recording demos:

  * Peek: can record part (or all of your screen) and turn it into a gif. Handy
    for including small demo clips in slides.
  * Flameshot: takes full or partial screenshots and making quick annotations.
