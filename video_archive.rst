.. _archive:

Video archive
=============

You can find `our video archive here`__.
If you prefer, you can also watch `our videos on YouTube`__.

__ https://meetings-archive.debian.net/pub/debian-meetings/
__ https://www.youtube.com/channel/UC7SbfAPZf8SMvAxp8t51qtQ

We are using automated tools to generate metadata from our video archive and
then use this data to upload the videos to YouTube.

* Our `metadata and scraping tools`__.
* Our `YouTube tools`__, and `their documentation`__.

__ https://salsa.debian.org/debconf-video-team/archive-meta
__ https://salsa.debian.org/debconf-video-team/youtube
__ https://debconf-video-team.pages.debian.net/youtube/
